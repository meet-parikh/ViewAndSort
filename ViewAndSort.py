from PySide.QtCore import *
from PySide.QtGui import *
import os

class ViewAndSortManupulation(QWidget):
    def __init__(self, parent=None):
        self.uncensorPath = ''
        self.censorPath = ''
        QWidget.__init__(self, parent)
        self.image_files = []
        self.counter = 1
        self.banner = QLabel('')
        self.label = QLabel('', self)
        self.label.setScaledContents(True)
        censorBtn = QPushButton("Censor")
        censorBtn.clicked.connect(self.censorAction)
        uncensorBtn = QPushButton("Uncensor")
        uncensorBtn.clicked.connect(self.uncensorAction)
        layout = QVBoxLayout()
        layout.addWidget(self.banner)
        layout.addWidget(self.label)
        hLayout = QHBoxLayout()
        hLayout.addWidget(censorBtn)
        hLayout.addWidget(uncensorBtn)
        layout.addLayout(hLayout)
        self.setLayout(layout)
        self.setFixedSize(700,500)

    def censorAction(self):
        newPath = os.path.join(self.censorPath, os.path.split(os.path.abspath(self.image_files[self.counter - 1]))[1])
        oldPath = self.image_files[self.counter - 1]
        os.rename(oldPath, newPath)
        self.counter += 1
        self.startImageSlides()

    def uncensorAction(self):
        newPath = os.path.join(self.uncensorPath, os.path.split(os.path.abspath(self.image_files[self.counter - 1]))[1])
        oldPath = self.image_files[self.counter - 1]
        os.rename(oldPath, newPath)
        self.counter += 1
        self.startImageSlides()

    def startImageSlides(self):
        self.banner.setText('Showing ' + str(self.counter) + ' of ' + str(len(self.image_files)))
        if self.counter > len(self.image_files):
            self.counter = 0
            self.image_files = []
            self.parent().setCurrentWidget(self.parent().selectorView)
        else:
            image_file = self.image_files[self.counter - 1]
            image = QPixmap(image_file)
            self.label.setPixmap(image)

class ViewAndSort(QWidget):
    """docstring for ViewAndSort"""
    def __init__(self, parent=None):
        self.defaultTxt = "None Selected"
        QWidget.__init__(self, parent)
        self.setGeometry(100, 100, 700, 500)
        homeBtn =  QPushButton("Open Home Directory") 
        self.homeLabel = QLabel(self.defaultTxt)
        censorBtn =  QPushButton("Open Censor Directory")
        self.censorLabel = QLabel(self.defaultTxt)
        uncensorBtn =  QPushButton("Open Uncensor Directory")
        self.uncensorLabel = QLabel(self.defaultTxt)
        self.startBtn = QPushButton("Start")
        homeBtn.clicked.connect(self.selectHomeDirectory)
        censorBtn.clicked.connect(self.selectCensorDirectory)
        uncensorBtn.clicked.connect(self.selectUncensorDirectory)
        layout = QVBoxLayout()
        layout.addWidget(homeBtn)
        layout.addWidget(self.homeLabel)
        layout.addWidget(censorBtn)
        layout.addWidget(self.censorLabel)
        layout.addWidget(uncensorBtn)
        layout.addWidget(self.uncensorLabel)
        layout.addWidget(self.startBtn)
        self.setLayout(layout)

    def selectHomeDirectory(self):
        flags = QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly
        directory = QFileDialog.getExistingDirectory(self, "Open Directory", os.getcwd(), flags)
        self.homeLabel.setText(directory)

    def selectCensorDirectory(self):
        flags = QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly
        directory = QFileDialog.getExistingDirectory(self, "Open Directory", os.getcwd(), flags)
        self.censorLabel.setText(directory)

    def selectUncensorDirectory(self):
        flags = QFileDialog.DontResolveSymlinks | QFileDialog.ShowDirsOnly
        directory = QFileDialog.getExistingDirectory(self, "Open Directory", os.getcwd(), flags)
        self.uncensorLabel.setText(directory)

class MainWindow(QStackedWidget):
    """docstring for MainWindow"""
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setGeometry(100, 100, 700, 500)
        self.selectorView = ViewAndSort(self)
        self.addWidget(self.selectorView)
        self.imageWindow = ViewAndSortManupulation(self)
        self.addWidget(self.imageWindow)
        self.selectorView.startBtn.clicked.connect(self.showManupulator)
        self.show()

    def showManupulator(self):
        if self.selectorView.defaultTxt not in [self.selectorView.homeLabel.text(), self.selectorView.censorLabel.text(), self.selectorView.uncensorLabel.text()]:
            for root, dirs, files in os.walk(self.selectorView.homeLabel.text()):
                for file in files:
                    if file.endswith(".jpg") or file.endswith(".JPG") or file.endswith(".png") or file.endswith(".PNG"):
                        self.imageWindow.image_files += [os.path.join(root, file)]
            self.imageWindow.uncensorPath = self.selectorView.uncensorLabel.text()
            self.imageWindow.censorPath = self.selectorView.censorLabel.text()
            self.imageWindow.startImageSlides()
            self.setCurrentWidget(self.imageWindow)

def main():
    app = QApplication([])
    mainWindow = MainWindow()  
    mainWindow.setWindowTitle("View And Sort Images")  
    app.exec_()

if __name__ == '__main__':
    main()